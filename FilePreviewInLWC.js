import { LightningElement, wire, track } from 'lwc';
import retriveFiles from '@salesforce/apex/LWCExampleController.retriveFiles';
import { NavigationMixin } from 'lightning/navigation';
export default class FilePrivewInLWC extends NavigationMixin(LightningElement) {
    @track files;

    @wire(retriveFiles)
    filesData({data, error}) {
        if(data) {
            window.console.log('data ===> '+data);
            this.files = data;
        }
        else if(error) {
            window.console.log('error ===> '+JSON.stringify(error));
        }
    }

  
    filePreview(event) {
        this[NavigationMixin.Navigate]({
            type: 'standard__namedPage',
            attributes: {
                pageName: 'filePreview'
            },
            state : {
                selectedRecordId:event.currentTarget.dataset.id
            }
          })
    }
}